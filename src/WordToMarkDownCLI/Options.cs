﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordToMarkDownCLI
{
    public class Options
    {
        [Option('f', "file", Required = false, HelpText = "Input file to be exported to markdown.")]
        public string File { get; set; }

        [Option('d', "directory", Required = false, HelpText = "Directory to scan for MS Word documents to export to markdown.")]
        public string Directory { get; set; }

        [Option('o', "outputDirectory", Required = false, HelpText = "Directory to output markdown files and content.")]
        public string OutputDirectory { get; set; }

        [Option('r', "recursive", Required = false, HelpText = "Recursive scan on the provided directory.", Default = false)]
        public bool Recursive { get; set; }

        [Option('m', "maintain", Required = false, HelpText = "Maintain folder structure.", Default = true)]
        public bool MaintainFolderStructure { get; set; }

        [Option('s', "summaryFile", Required = false, HelpText = "The path to the export summary file to be written.", Default = "")]
        public string ExportSummary { get; set; }
    }
}
