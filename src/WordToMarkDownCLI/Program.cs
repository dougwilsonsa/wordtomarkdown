﻿using CommandLine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using WordToMarkdown;

namespace WordToMarkDownCLI
{
  class Program
  {
    static void Main(string[] args)
    {
      var outputStringBuilder = new StringBuilder();
      var exporter = new WordToMarkDownExporter();
      Parser.Default.ParseArguments<Options>(args)
          .WithParsed<Options>(o =>
          {
            var outputDirectory = o.OutputDirectory.Length > 0 ? o.OutputDirectory : o.Directory;
            if (o.File != null)
            {
              if (!File.Exists(o.File))
              {
                Console.WriteLine($"{o.File} does not exist.");
                return;
              }
              Console.WriteLine($"Exporting {o.File}");
              exporter.Export(o.File, outputDirectory);
            }
            else if (o.Directory != null)
            {
              var dirInfo = new DirectoryInfo(o.Directory);
              if (!dirInfo.Exists)
              {
                Console.WriteLine($"{o.Directory} does not exist.");
                return;
              }

              List<FileInfo> filesToExport = new List<FileInfo>();
              filesToExport.AddRange(dirInfo.GetFiles("*.docx", o.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
              filesToExport.AddRange(dirInfo.GetFiles("*.dotx", o.Recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));

              foreach (var file in filesToExport)
              {
                string instanceOutputDirectory = outputDirectory;
                if (o.MaintainFolderStructure)
                {
                  var folderSubPart = file.FullName.Replace(dirInfo.FullName, "").Replace(file.Name, "");
                  instanceOutputDirectory = outputDirectory + folderSubPart;
                }
                WriteOutput($"Exporting {file}", outputStringBuilder);
                try
                {
                  exporter.Export(file.FullName, instanceOutputDirectory);
                }
                catch (Exception e)
                {
                  WriteOutput($"Could not export {file.Name}: {e.Message}", outputStringBuilder);
                }

              }
            }

            if (o.ExportSummary.Length > 0)
            {
              var fileInfo = new FileInfo(o.ExportSummary);
              if (!fileInfo.Directory.Exists)
              {
                Directory.CreateDirectory(fileInfo.DirectoryName);
              }
              File.WriteAllText(o.ExportSummary, outputStringBuilder.ToString());
            }
          });
    }

    static void WriteOutput(string message, StringBuilder outputSbStringBuilder)
    {
      Console.WriteLine(message);
      outputSbStringBuilder.AppendLine(message);
    }
  }
}
