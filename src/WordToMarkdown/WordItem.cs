﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordToMarkdown
{
    public class WordItem
    {
        public string StyleName { get; set; }
        public string Text { get; set; }
        public int Index { get; set; }
    }
}
