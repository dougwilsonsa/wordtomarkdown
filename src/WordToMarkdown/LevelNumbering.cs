﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordToMarkdown
{
    public class LevelNumbering
    {
        private List<Tuple<int, int>> _values = new List<Tuple<int, int>>();

        public int GetNextNumber(int indent)
        {
            var current = _values.FirstOrDefault(x => x.Item1 == indent);
            if (current == null)
            {
                current = new Tuple<int, int>(indent, 1);
                _values.Add(current);
            }
            else
            {
                current = new Tuple<int, int>(indent, current.Item2 + 1);
            }

            return current.Item2;
        }

        public void Reset()
        {
            _values = new List<Tuple<int, int>>();
        }
    }
}
