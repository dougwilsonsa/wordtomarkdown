﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xceed.Words.NET;
using Image = Xceed.Words.NET.Image;

namespace WordToMarkdown
{
    public class WordToMarkDownExporter
    {
        int _header1Count = 0;
        int _header2Count = 0;
        int _header3Count = 0;
        int _header4Count = 0;
        int _header5Count = 0;
        LevelNumbering _bulletNumbering = new LevelNumbering();

        /// <summary>
        /// The main export function to export a word document as a markdown file
        /// </summary>
        /// <param name="documentPath">The path to the word document</param>
        /// <param name="outputFolder">The output folder to write the markdown file to</param>
        public void Export(string documentPath, string outputFolder = "")
        {
            // Reset in case th class is being re-used
            ResetAll();

            // Check file exists and setup folders
            var fileInfo = new FileInfo(documentPath);
            if (!fileInfo.Exists)
            {
                return;
            }
            outputFolder = EnsureOutputFolder(outputFolder, fileInfo);
            var sanitizedFileName = GetSanitizedFilename(fileInfo.Name);
            var imageDirectory = Path.Combine(outputFolder, $"{sanitizedFileName}-images");
          
            // Load the word doc
            var sb = new StringBuilder();
            var docx = DocX.Load(fileInfo.FullName);
            var paragraphs = docx.Paragraphs.ToList();
            var paragraphStyleItems = paragraphs
                .Select(y => new WordItem()
                {
                    StyleName = y.StyleName,
                    Text = y.Text
                }).ToList();

            // Extract the images to disk
            ExtractAllImages(docx.Images, imageDirectory);

            // Merge title lines
            var titles = Utility.GetJoinedTitleItems(paragraphStyleItems);

            bool inTable = false;
            var tableRenderer = new TableRenderer();
            // Write the markdown file to string builder
            for (int i = 0; i < paragraphs.Count; i++)
            {
                // if its a table cell then push to the renderer
                if (paragraphs[i].ParentContainer == ContainerType.Cell)
                {
                    inTable = true;
                    tableRenderer.PushTableParagraph(paragraphs[i]);
                }
                else
                {
                    // if it was a table cell previously, but isn't any more then render the table before rendering this paragraph
                    if (inTable)
                    {
                        sb.Append(tableRenderer.GetTableMarkupString());
                        inTable = false;
                    }
                    RenderParagraph(paragraphs, i, titles, sb, sanitizedFileName);
                }
                
            }

            // export to file
            ExportMarkdownFile(outputFolder, fileInfo, sb);
        }

        private void RenderParagraph(List<Paragraph> paragraphs, int index, List<JoinedLineItem> titles, StringBuilder sb, string sanitizedFileName)
        {
            string headerNumbering = "";
            switch (paragraphs[index].StyleName)
            {
                case "Title":
                    var displayItem = titles.FirstOrDefault(x => x.JoinedIndexes[0] == index);
                    if (displayItem != null)
                    {
                        sb.AppendLine("# " + displayItem.Text);
                        sb.AppendLine();
                    }
                    _bulletNumbering.Reset();
                    break;
                case "Heading1":
                    headerNumbering = GetHeaderNumbering(1);
                    sb.AppendLine("## " + headerNumbering + paragraphs[index].Text);
                    sb.AppendLine();
                    _bulletNumbering.Reset();
                    break;
                case "Heading2":
                    headerNumbering = GetHeaderNumbering(2);
                    sb.AppendLine("### " + headerNumbering + paragraphs[index].Text);
                    sb.AppendLine();
                    _bulletNumbering.Reset();
                    break;
                case "Heading3":
                    headerNumbering = GetHeaderNumbering(3);
                    sb.AppendLine("#### " + headerNumbering + paragraphs[index].Text);
                    sb.AppendLine();
                    _bulletNumbering.Reset();
                    break;
                case "Heading4":
                    headerNumbering = GetHeaderNumbering(4);
                    sb.AppendLine("##### " + headerNumbering + paragraphs[index].Text);
                    sb.AppendLine();
                    _bulletNumbering.Reset();
                    break;
                case "Heading5":
                    headerNumbering = GetHeaderNumbering(5);
                    sb.AppendLine("###### " + headerNumbering + paragraphs[index].Text);
                    sb.AppendLine();
                    _bulletNumbering.Reset();
                    break;
                case "Normal":
                    foreach (var picture in paragraphs[index].Pictures)
                    {
                        sb.AppendLine($"![{picture.Name}]({sanitizedFileName}-images/{picture.FileName})");
                    }
                    sb.AppendLine(Utility.GetFormattedText(paragraphs[index]));
                    sb.AppendLine();
                    _bulletNumbering.Reset();
                    break;
                case "ListParagraph":
                    string indent = "";
                    for (int j = 0; j < paragraphs[index].IndentLevel; j++)
                    {
                        indent += "\t";
                    }

                    if (paragraphs[index].ListItemType == ListItemType.Bulleted)
                    {
                        sb.AppendLine(indent + "* " + Utility.GetFormattedText(paragraphs[index]));
                    }
                    else
                    {
                        sb.AppendLine(indent + _bulletNumbering.GetNextNumber(paragraphs[index].IndentLevel.Value) + ". " +
                                      Utility.GetFormattedText(paragraphs[index]));
                    }

                    break;
                default:
                    break;
            }
        }

        private static void ExportMarkdownFile(string outputFolder, FileInfo fileInfo, StringBuilder sb)
        {
            string mdPath = Path.Combine(outputFolder, fileInfo.Name.Replace(fileInfo.Extension, ".md"));
            if (File.Exists(mdPath))
            {
                File.Delete(mdPath);
            }

            File.WriteAllText(mdPath, sb.ToString());
        }

        private static string EnsureOutputFolder(string outputFolder, FileInfo fileInfo)
        {
            if (outputFolder.Length == 0)
            {
                outputFolder = fileInfo.Directory.FullName;
            }

            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
            }

            return outputFolder;
        }

        private void ResetAll()
        {
            _header1Count = 0;
            _header2Count = 0;
            _header3Count = 0;
            _header4Count = 0;
            _header5Count = 0;
            _bulletNumbering = new LevelNumbering();
        }

        

        private void ExtractAllImages(List<Image> images, string destinationPath)
        {
            var outputDirectory = new DirectoryInfo(destinationPath);
            if (!outputDirectory.Exists)
            {
                outputDirectory.Create();
            }

            foreach (var img in images)
            {
                ExtractImage(img, destinationPath);
            }
        }

        private void ExtractImage(Image image, string destinationPath)
        {
            if (image != null)
            {
                var fileparts = image.FileName.Split('.');
                var extension = fileparts[fileparts.Length - 1];
                var bitmap = new Bitmap(image.GetStream(FileMode.Open, FileAccess.Read));
                bitmap.Save(Path.Combine(destinationPath, image.FileName.Replace("." + extension, ".png")), ImageFormat.Png);
            }
        }

        private string GetSanitizedFilename(string filename)
        {
            return filename.Replace(" ", "_");
        }

        private string GetHeaderNumbering(int level)
        {
            var sb = new StringBuilder();
            switch (level)
            {
                case 1:
                    _header1Count++;
                    _header2Count = 0;
                    _header3Count = 0;
                    _header4Count = 0;
                    _header5Count = 0;
                    break;
                case 2:
                    _header2Count++;
                    _header3Count = 0;
                    _header4Count = 0;
                    _header5Count = 0;
                    break;
                case 3:
                    _header3Count++;
                    _header4Count = 0;
                    _header5Count = 0;
                    break;
                case 4:
                    _header4Count++;
                    _header5Count = 0;
                    break;
                case 5:
                    _header5Count++;
                    break;
                default:
                    break;
            }
            if (level > 0)
            {
                sb.Append(_header1Count + ".");
            }

            if (level > 1)
            {   
                sb.Append(_header2Count + ".");
            }

            if (level > 2)
            {   
                sb.Append(_header3Count + ".");
            }

            if (level > 3)
            {   
                sb.Append(_header4Count + ".");
            }

            if (level > 4)
            {
                sb.Append(_header5Count + ".");
            }

            sb.Append(" ");
            return sb.ToString();
        }

        private void ResetHeadings(int level)
        {
            switch (level)
            {
                case 1:
                    _header1Count++;
                    _header2Count = 0;
                    _header3Count = 0;
                    _header4Count = 0;
                    _header5Count = 0;
                    break;
                case 2:
                    _header2Count++;
                    _header3Count = 0;
                    _header4Count = 0;
                    _header5Count = 0;
                    break;
                case 3:
                    _header3Count++;
                    _header4Count = 0;
                    _header5Count = 0;
                    break;
                case 4:
                    _header4Count++;
                    _header5Count = 0;
                    break;
                case 5:
                    _header5Count = 0;
                    break;
                default:
                    break;
            }
        }
    }
}
