﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Words.NET;

namespace WordToMarkdown
{
    public class TableRenderer
    {
        DataTable _table = new DataTable();
        private int _lastCellColumnIndex = -1;
        private int _lastCellRowIndex = -1;

        public void PushTableParagraph(Paragraph paragraph)
        {
            bool isHeaderCell = IsHeaderCell(paragraph);
            if (isHeaderCell)
            {
                _table.Columns.Add(new DataColumn(paragraph.Text));
                _lastCellColumnIndex++;
            }
            else
            {
                // its a new row
                if ((_lastCellRowIndex == -1) || (_lastCellColumnIndex == _table.Columns.Count - 1))
                {
                    _lastCellRowIndex++;
                    _lastCellColumnIndex = 0;
                    _table.Rows.Add(_table.NewRow());
                }
                else
                {
                    _lastCellColumnIndex++;
                }
                _table.Rows[_lastCellRowIndex][_lastCellColumnIndex] = Utility.GetFormattedText(paragraph);
            }
        }

        private bool IsHeaderCell(Paragraph paragraph)
        {
            bool isbold = false;
            foreach (var magic in paragraph.MagicText)
            {
                if (magic.formatting != null)
                {
                    // its a header row
                    if ((magic.formatting.Bold != null) && magic.formatting.Bold.Value)
                    {
                        isbold = true;
                    }
                }
            }

            return isbold && (_lastCellRowIndex == -1);
        }

        public string GetTableMarkupString()
        {
            var sb = new StringBuilder("| ");
            foreach (DataColumn col in _table.Columns)
            {
                sb.Append($" {col.ColumnName} |");
            }
            sb.Append("\r\n|");
            for (int i = 0; i < _table.Columns.Count; i++)
            {
                sb.Append(" --- |");
            }
            for (int r = 0; r < _table.Rows.Count; r++)
            {
                sb.Append("\r\n|");
                for (int c = 0; c < _table.Columns.Count; c++)
                {
                    sb.Append($" {_table.Rows[r][c].ToString()} |");
                }
            }

            sb.AppendLine();
            return sb.ToString();
        }
    }
}
