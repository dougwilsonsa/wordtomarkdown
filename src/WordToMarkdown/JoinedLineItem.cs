﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WordToMarkdown
{
    public class JoinedLineItem
    {
        public JoinedLineItem()
        {
            Text = "";
            JoinedIndexes = new List<int>();
        }

        public string Text { get; set; }
        public List<int> JoinedIndexes { get; private set; }

    }
}
