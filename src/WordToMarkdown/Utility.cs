﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Xceed.Words.NET;

namespace WordToMarkdown
{
    public class Utility
    {
        public static string GetFormattedText(Paragraph paragraph)
        {
            var parts = new List<string>();
            int currentIndex = 0;
            foreach (var magic in paragraph.MagicText)
            {
                if (currentIndex < magic.index)
                {
                    parts.Add(paragraph.Text.Substring(currentIndex, magic.index - currentIndex));
                    currentIndex = magic.index + magic.text.Length;
                }

                string markDownFormattedText = GetMarkDownFormattedTextForMagic(magic, paragraph.Hyperlinks);
                parts.Add(markDownFormattedText);
                currentIndex = currentIndex + magic.text.Length;
            }

            var sb = new StringBuilder();
            foreach (var part in parts)
            {
                sb.Append(part);
            }

            return sb.ToString();
        }

        public static List<JoinedLineItem> GetJoinedTitleItems(List<WordItem> paragraphs)
        {
            var result = new List<JoinedLineItem>();
            var lastStyle = "";
            JoinedLineItem current = null;
            for (int i = 0; i < paragraphs.Count; i++)
            {
                if (!lastStyle.Equals("Title"))
                {
                    if (current != null)
                    {
                        result.Add(current);
                    }
                }

                if (paragraphs[i].StyleName.Equals("Title"))
                {
                    if (current == null)
                    {
                        current = new JoinedLineItem();
                    }
                    current.JoinedIndexes.Add(i);
                    current.Text += (current.Text.Length > 0) ? " " + paragraphs[i].Text : paragraphs[i].Text;
                    if (i == paragraphs.Count - 1)
                    {
                        result.Add(current);
                    }
                }
                else
                {
                    if (current != null)
                    {
                        result.Add(current);
                        current = null;
                    }
                }

                lastStyle = paragraphs[i].StyleName;
            }

            return result;
        }

        private static string GetMarkDownFormattedTextForMagic(FormattedText magicText, List<Hyperlink> hyperlinks)
        {
            string result = magicText.text;
            if (magicText.formatting != null)
            {
                // hyperlinks
                if (magicText.formatting.StyleName != null && magicText.formatting.StyleName.Equals("Hyperlink"))
                {
                    var hyperlink = hyperlinks.FirstOrDefault(x => x.Text != null && x.Text.Equals(magicText.text));
                    if (hyperlink != null)
                    {
                        if (hyperlink.Uri != null && hyperlink.Uri.AbsoluteUri != null)
                        {
                            result = $"[{result}]({hyperlink.Uri.AbsoluteUri})";
                        }
                    }
                }

                // bold and italics
                if ((magicText.formatting.Bold != null) && (magicText.formatting.Italic != null) && magicText.formatting.Bold.Value && magicText.formatting.Italic.Value)
                {
                    result = $"***{result}***";
                }
                // bold
                else if ((magicText.formatting.Bold != null) && magicText.formatting.Bold.Value)
                {
                    result = $"**{result}**";
                }
                // italics
                else if ((magicText.formatting.Italic != null) && magicText.formatting.Italic.Value)
                {
                    result = $"*{result}*";
                }
            }

            return result;
        }
    }
}
