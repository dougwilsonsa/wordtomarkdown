﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WordToMarkdown;
using Xceed.Words.NET;

namespace WordToMarkdownTests
{
    [TestClass]
    public class UtilityTests
    {
        [TestMethod]
        public void TestGetJoinedTitleItems()
        {
            var items = new List<WordItem>();
            items.Add(new WordItem() { StyleName = "Title", Text = "Title 1"});
            items.Add(new WordItem() { StyleName = "Title", Text = "Title 2" });
            items.Add(new WordItem() { StyleName = "Not", Text = "Not" });
            items.Add(new WordItem() { StyleName = "Title", Text = "Title 3" });
            items.Add(new WordItem() { StyleName = "Not", Text = "Not" });
            items.Add(new WordItem() { StyleName = "Title", Text = "Title 4" });
            items.Add(new WordItem() { StyleName = "Title", Text = "Title 5" });
            items.Add(new WordItem() { StyleName = "Title", Text = "Title 6" });
            items.Add(new WordItem() { StyleName = "Not", Text = "Not" });
            items.Add(new WordItem() { StyleName = "Title", Text = "Title 7" });
            var output = Utility.GetJoinedTitleItems(items);
            Assert.AreEqual(output.Count, 4, "Correct number of joins returned");
            Assert.AreEqual(output[0].JoinedIndexes.Count, 2, "Join has the correct number of items");
            Assert.AreEqual(output[1].JoinedIndexes.Count, 1, "Join has the correct number of items");
            Assert.AreEqual(output[2].JoinedIndexes.Count, 3, "Join has the correct number of items");
            Assert.AreEqual(output[3].JoinedIndexes.Count, 1, "Join has the correct number of items");
            Assert.AreEqual(output[0].JoinedIndexes[0], 0, "Join has the correct items");
            Assert.AreEqual(output[0].JoinedIndexes[1], 1, "Join has the correct items");
            Assert.AreEqual(output[1].JoinedIndexes[0], 3, "Join has the correct items");
            Assert.AreEqual(output[2].JoinedIndexes[0], 5, "Join has the correct items");
            Assert.AreEqual(output[2].JoinedIndexes[1], 6, "Join has the correct items");
            Assert.AreEqual(output[2].JoinedIndexes[2], 7, "Join has the correct items");
            Assert.AreEqual(output[3].JoinedIndexes[0], 9, "Join has the correct items");
        }
    }
}
