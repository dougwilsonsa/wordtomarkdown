# WordToMarkDown

Exports word documents to markdown files (including images, tables, bullets, numbered headers, bold, italics). Class library and CLI.

## Download

[Download WordToMarkDownCLI](https://gitlab.com/dougwilsonsa/wordtomarkdown/-/jobs/artifacts/master/download?job=build)

## Command Line Usage

### Help

```cmd
WordToMarkDownCLI.exe --help
```

### Export a single file

```cmd
WordToMarkDownCLI.exe -f "C:\MyWordDocumentsFolder\MyWordDocument.docx" -o "C:\MyOutputFolder"
```

### Export a directory of files

#### Exporting documents from a single folder

```cmd
WordToMarkDownCLI.exe -d "C:\MyWordDocumentsFolder" -o "C:\MyOutputFolder" 
```

#### Exporting documents from a folder and subfolders

```cmd
WordToMarkDownCLI.exe -d "C:\MyWordDocumentsFolder" -o "C:\MyOutputFolder" -r true
```

By default, when using recursion (including subfolder), the tool will maintain the folder structure of the source directory (i.e. will create subfolder that mirror the source folder). 

To disable this set ```-m``` to ```false```.

```cmd
WordToMarkDownCLI.exe -d "C:\MyWordDocumentsFolder" -o "C:\MyOutputFolder" -r true -m false
```

To write an export summary file (mirrors console output) provide a path to the file to write with the ```-s``` switch.

```cmd
WordToMarkDownCLI.exe -d "C:\MyWordDocumentsFolder" -o "C:\MyOutputFolder" -s "C:\exportsummary.txt"
```

## Class Usage

Add a reference to WordToMarkDown.dll (in the download zip)

```c#
var exporter = new WordToMarkDownExporter();
exporter.Export(pathToWordDocument, outputDirectory);
```
